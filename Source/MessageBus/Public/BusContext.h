#pragma once

#include "Engine.h"
#include "Tickable.h"
#include "Http.h"
#include "Json.h"
#include "StructSerializer.h"
#include "BusContext.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMessageReceived, FString, ChannelName, FString, Message);

UCLASS(BlueprintType, Blueprintable)
class UBusContext : public UObject, public FTickableGameObject
{
	GENERATED_UCLASS_BODY()

	void Init(); 

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Subscribe"), Category = "MessageBus")
	void Subscribe(const FString& ChannelName); 

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Publish"), Category = "MessageBus")
	void Publish(const FString& ChannelName, const FString& Message);   

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Unsubscribe"), Category = "MessageBus")
	void Unsubscribe(const FString& ChannelName);
		
	UFUNCTION(BlueprintCallable, meta = (DisplayName = "UnsubscribeAll"), Category = "MessageBus")
	void UnsubscribeAll();

	virtual bool IsTickableWhenPaused() const override;
	virtual bool IsTickableInEditor() const override;
	virtual void Tick(float DeltaTime) override;
	virtual bool IsTickable() const override;
	virtual TStatId GetStatId() const override;

	UPROPERTY(BlueprintAssignable)
	FOnMessageReceived MessageRecieved; 

private:

	void Subscribe(const FString& ChannelName, FString TimeToken);

	FString AuthenticationTokenPub, AuthenticationTokenSub;
	FString UniqueIdentifier; 
	TMap<FString, FHttpRequestPtr>  Subscriptions;
	TSet<FString> CancelledSubscriptions;

	// HTTP response handlers
	void OnPublishResponse(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded);
	void OnSubscribeResponse(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded, FString ChannelName);


public:
	virtual void BeginDestroy() override;

};
