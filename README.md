# MessageBus
Unreal Engine MessageBus Plugin (cloned from https://github.com/ankitkk/MessageBus)

Provides a Message Bus interface for publish-subsribe mechanism for Unreal Engine 4.

 - allows games instances to communicate without unreal engine server.
 - uses https://www.pubnub.com/ as a backend.
 - more backends to come: Amazon SQS, Pusher (https://pusher.com/docs/channels/library_auth_reference/rest-api)

Copyright © 2016 Ankit Khare ALL RIGHTS RESERVED
