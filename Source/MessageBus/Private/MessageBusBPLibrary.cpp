#include "MessageBusPrivatePCH.h"
#include "MessageBusBPLibrary.h"

UMessageBusBPLibrary::UMessageBusBPLibrary(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{

}


UBusContext* UMessageBusBPLibrary::CreateMessageBusContext()
{
    UBusContext* Context = NewObject<UBusContext>();
    Context->Init(); 
    return Context; 
}

