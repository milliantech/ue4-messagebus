#pragma once

#include "CoreMinimal.h"
#include "MessageBusSettings.generated.h"

UCLASS(config = Engine, defaultconfig)
class MESSAGEBUS_API UMessageBusSettings : public UObject
{
	GENERATED_UCLASS_BODY()
	
public:
	
	UPROPERTY(Config, EditAnywhere, Category = PubNub)
	FString PublishKey;
	
	UPROPERTY(Config, EditAnywhere, Category = PubNub)
	FString SubscribeKey;

};
