#include "MessageBusPrivatePCH.h"
#include "BusContext.h"
#include "MessageBusSettings.h"

// Unreal JSON support is incomplete and inconsistent. 
#include "json11.hpp"

#include "OnlineSubSystem.h"
#include "OnlineIdentityInterface.h"
#include "DateTime.h"
#include "Base64.h"



#include "JsonStructDeserializerBackend.h"
#include "JsonStructSerializerBackend.h"
#include "StructDeserializer.h"
#include "StructSerializer.h"

// REST calls are mostly undocumented. 
// https://www.pubnub.com/http-rest-push-api/
// https://raw.githubusercontent.com/pubnub/unity/6f9c1a4aba72bc382de84973f9d45be671f7e0dc/Assets/Scripts/Pubnub/BuildRequests.cs

UBusContext::UBusContext(class FObjectInitializer const&)
{}

void UBusContext::Init()
{
	// Fetch tokens from plugin settings
	const UMessageBusSettings* settings = GetDefault<UMessageBusSettings>();
	AuthenticationTokenPub = settings->PublishKey;
	AuthenticationTokenSub = settings->SubscribeKey;

	// Using id from device
	// TODO using id provided from game
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get("Null");
	IOnlineIdentityPtr IdentityInt = OnlineSub->GetIdentityInterface();
	UniqueIdentifier = IdentityInt->GetUniquePlayerId(0)->ToString();
}

void UBusContext::Subscribe(const FString& ChannelName)
{
	Subscribe(ChannelName, "0");
}

void UBusContext::Subscribe(const FString& ChannelName, FString TimeToken)
{
	// subscribe chain.
	FString Callback("0");

	FString SubUrl = FString("https://ps.pndsn.com/subscribe/")
		+ AuthenticationTokenSub + "/"
		+ ChannelName + "/"
		+ Callback + "/"
		+ TimeToken + "?uuid="+ UniqueIdentifier;
	UE_LOG(LogMessageBus, Verbose, TEXT(" Subscribe URL is %s"), *SubUrl);

	FHttpModule& HttpModule = FHttpModule::Get();
	TSharedRef<class IHttpRequest> HttpRequest = HttpModule.Get().CreateRequest();

	// kick off http request to read
	HttpRequest->OnProcessRequestComplete().BindUObject(this, &UBusContext::OnSubscribeResponse, ChannelName);
	HttpRequest->SetURL(SubUrl);
	HttpRequest->SetHeader(TEXT("Content-Type"), TEXT("text/html; charset=utf-8"));
	HttpRequest->SetVerb(TEXT("GET"));
	HttpRequest->ProcessRequest();

	// keep track of existing subscriptions.
	Subscriptions.Add(ChannelName, HttpRequest);
}

void UBusContext::Unsubscribe(const FString& ChannelName)
{
	FHttpRequestPtr* Req = Subscriptions.Find(ChannelName);
	if (Req)
	{
		UE_LOG(LogMessageBus, Verbose, TEXT("Unsubscribing channel %s"), *ChannelName);

		CancelledSubscriptions.Add(ChannelName);

		(*Req)->CancelRequest();
		Subscriptions.Remove(ChannelName);
	}
	else
	{
		UE_LOG(LogMessageBus, Verbose, TEXT("There's no channel named %s"), *ChannelName);
	}
}

void UBusContext::UnsubscribeAll()
{
	for (auto it = Subscriptions.CreateConstIterator(); it; ++it)
	{
		Unsubscribe(it->Key);
	}
}

void UBusContext::Publish(const FString& ChannelName, const FString& Message)
{
    if (Message.Len() > 32000)
    {
        UE_LOG(LogMessageBus, Error, TEXT(" Message Size Too Big "));
    }

	FString Signature("0");
	FString CallBack("0"); 
	FString EncodedMessage = FGenericPlatformHttp::UrlEncode(Message);

	FString PubUrl = FString("https://ps.pndsn.com/publish/")
		+ AuthenticationTokenPub + "/"
		+ AuthenticationTokenSub + "/"
		+ Signature + "/"
		+ ChannelName + "/"
		+ CallBack + "/"
		+ EncodedMessage;

	FHttpModule& HttpModule = FHttpModule::Get();
	TSharedRef<class IHttpRequest> HttpRequest = HttpModule.Get().CreateRequest();

	// kick off http request to read
	HttpRequest->OnProcessRequestComplete().BindUObject(this, &UBusContext::OnPublishResponse);
	HttpRequest->SetURL(PubUrl);
	HttpRequest->SetHeader(TEXT("Content-Type"), TEXT("text/html; charset=utf-8"));
	HttpRequest->SetVerb(TEXT("GET"));
	HttpRequest->ProcessRequest();
}

bool UBusContext::IsTickableWhenPaused() const
{
	return false;
}

bool UBusContext::IsTickableInEditor() const
{
	return false;
}

void UBusContext::Tick(float DeltaTime)
{
}

bool UBusContext::IsTickable() const
{
	return false;
}

TStatId UBusContext::GetStatId() const
{
	return TStatId();
}

void UBusContext::BeginDestroy()
{
	Super::BeginDestroy(); 
	for (auto It = Subscriptions.CreateConstIterator(); It; ++It)
	{
		(*It.Value()).OnProcessRequestComplete().BindLambda([=](FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded) {});
		(*It.Value()).CancelRequest();
	}
}

void UBusContext::OnPublishResponse(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded)
{
	if (bSucceeded)
	{
		UE_LOG(LogMessageBus, Verbose, TEXT("Publish request response *%s"), *HttpResponse->GetContentAsString());
	}
}

void UBusContext::OnSubscribeResponse(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded, FString ChannelName)
{
	FString SeqNum("0");
	if (CancelledSubscriptions.Find(ChannelName)) {
		UE_LOG(LogMessageBus, Verbose, TEXT("Unsubscribing channel %s"), *ChannelName);

		CancelledSubscriptions.Remove(ChannelName);

		return;
	}

	if (bSucceeded)
	{
		FString Body = HttpResponse->GetContentAsString();
		UE_LOG(LogMessageBus, Verbose, TEXT(" Subscribe %s  *%s"), *ChannelName, *Body);

		std::string err;
		auto json = json11::Json::parse(std::string(TCHAR_TO_UTF8(*Body)), err);

		if (json.array_items()[0].is_array() && json.array_items()[0].array_items().size())
		{
			/*
				Received message sample
			[
				[
					{
						"text":"Enter Message Here"
					}
				],
				"15768041939029479"
			]
			*/

			auto messagesArr = json.array_items()[0];
			for (auto msg : messagesArr.array_items())
			{
				auto innerMsg = msg[0];
				MessageRecieved.Broadcast(ChannelName, ANSI_TO_TCHAR(innerMsg.dump().c_str()));

				/*
				switch (msg.type)
				{
				case json11::Json::Type::NUL:

					break;

				case json11::Json::Type::NUMBER:

					break;

				case json11::Json::Type::BOOL:

					break;

				case json11::Json::Type::STRING:

					break;

				case json11::Json::Type::ARRAY:

					break;

				case json11::Json::Type::OBJECT:

					break;
				}
				*/
			}

			// Better handling, parsing Presence channel
			/*
			FString Data(ANSI_TO_TCHAR(json.array_items()[0].array_items()[0].string_value().c_str()));
			if (ChannelName.Contains("-pnpres"))
			{

			}
			else
			{
				MessageRecieved.Broadcast(Data);
			}
			*/

			/*
			else if (ChannelName.Contains("-struct"))
			{
				// this was in response to a struct publish.
				// lets deserialize it.
				FMessageCapsule   RecievedStruct;
				FStructDeserializerPolicies Policies;
				Policies.MissingFields = EStructDeserializerErrorPolicies::Warning;

				TArray<uint8> Buffer;
				// Decode the string.
				FBase64::Decode(Data, Buffer);
				FMemoryReader Reader(Buffer);

				FJsonStructDeserializerBackend DeserializerBackend(Reader);

				if (FStructDeserializer::Deserialize(RecievedStruct, DeserializerBackend, Policies))
				{
					MessageStructRecieved.Broadcast(RecievedStruct);
				}
				else
				{
					UE_LOG(LogMessageBus, Warning, TEXT(" Message Recieved but could not de-serialize into FMessageStruct"));
				}
			}
			*/

		}
		SeqNum = FString(ANSI_TO_TCHAR(json.array_items()[1].string_value().c_str()));
		UE_LOG(LogMessageBus, Verbose, TEXT(" Next SeqNum %s"), *SeqNum);
	}

	// Call again to wait for messages
	Subscribe(ChannelName, SeqNum);
}